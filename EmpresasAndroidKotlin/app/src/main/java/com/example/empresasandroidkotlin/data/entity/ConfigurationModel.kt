package com.example.empresasandroidkotlin.data.entity

import com.google.gson.annotations.SerializedName

data class ConfigurationModel(
    @SerializedName("accessToken") var accessToken : String = "",
    @SerializedName("client") var client : String = "",
    @SerializedName("uid") var uid : String = ""
)