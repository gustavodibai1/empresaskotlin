package com.example.empresasandroidkotlin.view.listeners

import com.example.empresasandroidkotlin.data.entity.User
import retrofit2.Response

interface GetUsersCallback {
    fun onSuccess(response : Response<User>)
    fun onError()
}