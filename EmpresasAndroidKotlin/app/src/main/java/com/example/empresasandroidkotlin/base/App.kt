package com.example.empresasandroidkotlin.base

import android.app.Application
import com.example.empresasandroidkotlin.dagger.ApplicationComponent
import com.example.empresasandroidkotlin.dagger.DaggerApplicationComponent
import com.example.empresasandroidkotlin.dagger.ApplicationModule
import com.example.empresasandroidkotlin.modules.androidUtilsModule
import com.example.empresasandroidkotlin.modules.repositoryModule
import com.example.empresasandroidkotlin.modules.viewModelModule
import org.koin.android.ext.android.startKoin

open class App : Application() {

    val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(viewModelModule, repositoryModule, androidUtilsModule))
    }

}