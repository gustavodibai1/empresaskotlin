package com.example.empresasandroidkotlin.data.entity

import com.google.gson.annotations.SerializedName

data class Enterprise(
    @SerializedName("id") val id : Long = 0L,
    @SerializedName("enterprise_name") val enterpriseName : String,
    @SerializedName("description") val description : String,
    @SerializedName("photo") val photo : String,
    @SerializedName("country") val country : String,
    @SerializedName("enterprise_type") val enterpriseType : EnterpriseTypeModel
)