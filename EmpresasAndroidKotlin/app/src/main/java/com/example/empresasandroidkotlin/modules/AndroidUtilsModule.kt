package com.example.empresasandroidkotlin.modules

import com.example.empresasandroidkotlin.util.ConnectivityHelper
import com.example.empresasandroidkotlin.util.SharedPreferencesHelper
import org.koin.dsl.module.module

val androidUtilsModule = module {

    single {
        SharedPreferencesHelper(get())
    }

    single {
        ConnectivityHelper(get())
    }

}