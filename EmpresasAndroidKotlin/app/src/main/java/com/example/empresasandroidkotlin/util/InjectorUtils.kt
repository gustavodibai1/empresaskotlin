package com.example.empresasandroidkotlin.util

import com.example.empresasandroidkotlin.data.repository.EnterpriseRepository
import com.example.empresasandroidkotlin.data.repository.UserRepository
import com.example.empresasandroidkotlin.view.ui.login.LoginViewModelFactory
import com.example.empresasandroidkotlin.view.ui.main.EnterpriseViewModelFactory

object InjectorUtils {
    fun doLoginViewModelFactory(helper: SharedPreferencesHelper, connectivityHelper: ConnectivityHelper) : LoginViewModelFactory? {
        val userRepository =
            UserRepository(helper, connectivityHelper)
        return LoginViewModelFactory(userRepository)
    }

    fun doEnterpriseViewModelFactory() : EnterpriseViewModelFactory? {
        val enterpriseRepository = EnterpriseRepository()
        return EnterpriseViewModelFactory(enterpriseRepository)
    }
}