package com.example.empresasandroidkotlin.data.entity

import com.example.empresasandroidkotlin.data.entity.Enterprise

data class ResponseEnterprise(
    val enterprises: MutableList<Enterprise>
)