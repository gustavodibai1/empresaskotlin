package com.example.empresasandroidkotlin.view.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.example.empresasandroidkotlin.R
import com.example.empresasandroidkotlin.view.ui.login.LoginActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed(
            object : Runnable {
                override fun run() {
                    mostrarLogin()
                }
            }, 2000)
    }

        fun mostrarLogin() {
            val intent = Intent(this@SplashActivity, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
}
