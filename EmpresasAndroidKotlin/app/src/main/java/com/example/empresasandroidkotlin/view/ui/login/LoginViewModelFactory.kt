package com.example.empresasandroidkotlin.view.ui.login

import androidx.lifecycle.ViewModelProvider
import com.example.empresasandroidkotlin.data.repository.UserRepository

class LoginViewModelFactory(private val userRepository: UserRepository) : ViewModelProvider.Factory {
    override fun <T : androidx.lifecycle.ViewModel?> create(modelClass: Class<T>): T {
        return LoginViewModel(userRepository) as T
    }
}