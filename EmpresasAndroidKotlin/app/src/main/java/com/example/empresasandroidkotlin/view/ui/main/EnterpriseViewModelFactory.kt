package com.example.empresasandroidkotlin.view.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.empresasandroidkotlin.data.repository.EnterpriseRepository

class EnterpriseViewModelFactory(private val enterpriseRepository: EnterpriseRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return EnterpriseViewModel(enterpriseRepository) as T
    }
}