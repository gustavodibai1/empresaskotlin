package com.example.empresasandroidkotlin.dagger

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.empresasandroidkotlin.view.ui.main.EnterpriseViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory : ViewModelFactory) : ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(EnterpriseViewModel::class)
    abstract fun bindsEnterpriseViewModel(enterpriseViewModel: EnterpriseViewModel) : ViewModel
}