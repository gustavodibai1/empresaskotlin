package com.example.empresasandroidkotlin.data.repository

import com.example.empresasandroidkotlin.data.Result
import com.example.empresasandroidkotlin.data.entity.Enterprise
import com.example.empresasandroidkotlin.data.entity.ResponseEnterprise
import com.example.empresasandroidkotlin.data.rest.RetrofitInstance
import com.example.empresasandroidkotlin.util.SharedPreferencesHelper
import java.lang.Exception
import javax.inject.Inject

class EnterpriseRepository @Inject constructor(){

    suspend fun mostrarEmpresas(helper: SharedPreferencesHelper
    ) : Result<MutableList<Enterprise>> {
        val configurationModel = helper.getConfigurationModel()

        return try {
            val response : ResponseEnterprise = RetrofitInstance().getAPI().getEmpresas(
                configurationModel.accessToken,
                configurationModel.client,
                configurationModel.uid
            ).await()

            val enterpriseList = response.enterprises

            Result.Success(enterpriseList)

        } catch(e : Exception) {
            Result.Error(e)
        }

//        call.enqueue(object : Callback<ResponseEnterprise> {
//            override fun onResponse(call: Call<ResponseEnterprise>?, response: Response<ResponseEnterprise>) {
//                if(response.isSuccessful){
//                    val enterpriseList = response.body()!!.enterprises
//
//                    callback.onSuccess(enterpriseList)
//
//                } else {
//                    callback.onError(Exception("Error!"))
//                }
//            }
//
//            override fun onFailure(call: Call<ResponseEnterprise>?, t: Throwable?) {
//                callback.onError(Exception("Error!"))
//            }
//        }
//        )
    }
}