package com.example.empresasandroidkotlin.data.entity

import com.google.gson.annotations.SerializedName

data class EnterpriseTypeModel (
    @SerializedName("id") val id : Long = 0L,
    @SerializedName("enterprise_type_name") val typeName : String = ""
)
