package com.example.empresasandroidkotlin.view.ui.empresa

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.example.empresasandroidkotlin.R
import com.example.empresasandroidkotlin.data.entity.ConfigurationModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_empresa.*

class EmpresaActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_empresa)

        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        var pathCover : String? = intent.getStringExtra("enterprisePhoto")
        var enterpriseName : String = intent.getStringExtra("enterpriseName")
        var description : String = intent.getStringExtra("enterpriseDescription")

        txt_title_empresa.text = enterpriseName
        txt_description.text = description

        val progressDrawable = CircularProgressDrawable(this)
        progressDrawable.strokeWidth = 5f
        progressDrawable.centerRadius = 30f
        progressDrawable.start()

        Glide.with(this)
            .load(getString(R.string.BASE_URL) + pathCover)
            .fitCenter()
            .placeholder(progressDrawable)
            .error(R.drawable.img_e_1_lista)
            .into(img_logo_empresa)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
