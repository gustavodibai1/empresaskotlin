package com.example.empresasandroidkotlin.data.repository

import android.accounts.NetworkErrorException
import com.example.empresasandroidkotlin.data.Result
import com.example.empresasandroidkotlin.data.entity.ConfigurationModel
import com.example.empresasandroidkotlin.data.entity.User
import com.example.empresasandroidkotlin.data.rest.RetrofitInstance
import com.example.empresasandroidkotlin.util.ConnectivityHelper
import com.example.empresasandroidkotlin.util.SharedPreferencesHelper
import java.lang.Exception

class UserRepository(
    private val helper: SharedPreferencesHelper,
    private val connectivityHelper: ConnectivityHelper
) {

    suspend fun logar(user: User): Result<ConfigurationModel> {

        if (connectivityHelper.verificaConexao()) {
            return Result.NetworkError(NetworkErrorException())
        } else {
            val configurationModel = ConfigurationModel()
            val instance = RetrofitInstance()

            return try {
                val response = instance.getAPI().loginUsuario(user).await()
                configurationModel.accessToken = response.headers().get("access-token")!!
                configurationModel.client = response.headers().get("client")!!
                configurationModel.uid = response.headers().get("uid")!!
                helper.saveConfigurationModel(configurationModel)

                Result.Success(configurationModel)
            } catch (exception: Exception) {
                Result.Error(exception)
            }

//            instance.getAPI().loginUsuario(user).enqueue(object : Callback<User> {
//                override fun onResponse(call: Call<User>, response: Response<User>) {
//                    if (response.isSuccessful) {
//                        configurationModel.accessToken = response.headers().get("access-token")!!
//                        configurationModel.client = response.headers().get("client")!!
//                        configurationModel.uid = response.headers().get("uid")!!
//                        helper.saveConfigurationModel(configurationModel)
//
//                        data.value = (Result.Success(configurationModel))
//                    } else {
//                        data.value = (Result.Error(Throwable(response.message())))
//                    }
//                }
//
//                override fun onFailure(call: Call<User>, t: Throwable) {
//                    data.value = (Result.Error(t))
//                }
//            })
        }

    }
}