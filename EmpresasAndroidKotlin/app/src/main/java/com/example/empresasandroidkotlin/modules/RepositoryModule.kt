package com.example.empresasandroidkotlin.modules

import com.example.empresasandroidkotlin.data.repository.EnterpriseRepository
import com.example.empresasandroidkotlin.data.repository.UserRepository
import org.koin.dsl.module.module

val repositoryModule = module {

    factory { UserRepository(get(), get()) }

    factory { EnterpriseRepository() }

}