package com.example.empresasandroidkotlin.dagger

import com.example.empresasandroidkotlin.base.App
import com.example.empresasandroidkotlin.base.BaseActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ApplicationModule::class,
    ViewModelModule::class
])interface ApplicationComponent {

    fun inject(application: App)
    fun inject(activity: BaseActivity)
}