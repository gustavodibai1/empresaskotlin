package com.example.empresasandroidkotlin.view

class ViewState<D, S>(
    val status: S,
    val data: D? = null,
    val error: Throwable? = null
)