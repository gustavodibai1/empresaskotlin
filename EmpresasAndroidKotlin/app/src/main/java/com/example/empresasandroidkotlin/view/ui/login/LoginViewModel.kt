package com.example.empresasandroidkotlin.view.ui.login

import androidx.lifecycle.*
import com.example.empresasandroidkotlin.data.repository.UserRepository
import com.example.empresasandroidkotlin.data.entity.User
import com.example.empresasandroidkotlin.data.entity.ConfigurationModel
import  com.example.empresasandroidkotlin.data.Result
import com.example.empresasandroidkotlin.view.ViewState
import  com.example.empresasandroidkotlin.view.ui.login.LoginViewState.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class LoginViewModel() : ViewModel() {

    //    val observable: MutableLiveData<Result<ConfigurationModel>> = MutableLiveData()
    val loginState: MutableLiveData<ViewState<ConfigurationModel, LoginViewState>> = MutableLiveData()
    private lateinit var repo: UserRepository

    constructor(repo: UserRepository) : this() {
//        addObserverLoginApiCall()
        this.repo = repo
    }

    fun observeLoginState(): MutableLiveData<ViewState<ConfigurationModel, LoginViewState>> = loginState

    fun fazerLogin(user: User) {

        if (isValidUserCredentials(user)) {
            loginState.postValue(ViewState(LOADING))
            GlobalScope.launch(Dispatchers.IO) {
                val result = repo.logar(user)
                withContext(Dispatchers.Main) {
                    when (result) {
                        is Result.Success -> loginState.postValue(
                            ViewState(
                                SUCCESS,
                                result.data
                            )
                        )
                        is Result.Error -> loginState.postValue(
                            ViewState(
                                ERROR,
                                error = result.exception
                            )
                        )
                        is Result.NetworkError -> loginState.postValue(ViewState(NETWORK_ERROR))
                    }
                }
            }
        }
    }

//    private fun addObserverLoginApiCall() {
//        observable.observeForever {
//
//        }
//    }

    private fun isValidUserCredentials(user: User): Boolean {
        if (user.email.isEmpty()) {
            loginState.postValue(ViewState(EMPTY_EMAIL))
            return false
        }
        if (user.password.isEmpty()) {
            loginState.postValue(ViewState(EMPTY_PASSWORD))
            return false
        }

        return true
    }
}