package com.example.empresasandroidkotlin.view.ui.main

import  com.example.empresasandroidkotlin.view.ui.main.EnterpriseViewState.*

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.empresasandroidkotlin.R
import com.example.empresasandroidkotlin.base.BaseActivity
import com.example.empresasandroidkotlin.view.ui.adapter.RecyclerAdapter
import com.example.empresasandroidkotlin.data.entity.Enterprise
import com.example.empresasandroidkotlin.view.listeners.EnterpriseClickListener
import com.example.empresasandroidkotlin.view.listeners.GetEmpresasCallback
import com.example.empresasandroidkotlin.view.ui.empresa.EmpresaActivity
import com.example.empresasandroidkotlin.util.SharedPreferencesHelper
import kotlinx.android.synthetic.main.activity_main.*
import com.example.empresasandroidkotlin.dagger.viewModel

class MainActivity : BaseActivity(), EnterpriseClickListener{

    override fun clickItem(view: View?, enterprise: Enterprise) {
        val intent = Intent(this, EmpresaActivity::class.java)
        intent.putExtra("enterprisePhoto", enterprise.photo)
        intent.putExtra("enterpriseName", enterprise.enterpriseName)
        intent.putExtra("enterpriseDescription", enterprise.description)
        startActivity(intent)
    }

    lateinit var adapter : RecyclerAdapter
    lateinit var helper : SharedPreferencesHelper
    private lateinit var viewModel: EnterpriseViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = viewModel(viewModelFactory)

        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayShowTitleEnabled(false)

        adapter = RecyclerAdapter(this)
        helper = SharedPreferencesHelper(this)

        progress_bar.visibility = View.GONE

        recyclerView.visibility = View.INVISIBLE

        val linearLayoutManager : LinearLayoutManager? = LinearLayoutManager(this)

        recyclerView.adapter = adapter
        recyclerView.layoutManager = linearLayoutManager
        adapter.setClickListener(this@MainActivity)

        viewModel.mostrarEmpresas(helper)
    }

    override fun onResume() {
        super.onResume()

        if(!viewModel.observeEnterpriseState().hasObservers()) {
            viewModel.observeEnterpriseState().observe(this, Observer {
                when (it.status) {
                    LOADING -> progress_bar.visibility = View.VISIBLE
                    SUCCESS -> onSuccessLoading(it.data)
                    ERROR -> onError(it.error)
                    NETWORK_ERROR -> onNetworkError()
                }
            })
        }
    }

    private fun onNetworkError() {
        Toast.makeText(this@MainActivity, "Error de conexão!", Toast.LENGTH_SHORT).show()
    }

    private fun onError(error: Throwable?) {
        error!!.printStackTrace()
    }

    private fun onSuccessLoading(data: MutableList<Enterprise>?) {
        adapter.setAdapter(data!!)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu, menu)

        val searchItem = menu.findItem(R.id.action_search)

        val mSearchView = searchItem.actionView as SearchView
        mSearchView.queryHint = getString(R.string.hintSearchViewHome)

        mSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                txtHome.visibility = View.INVISIBLE
                recyclerView.visibility = View.VISIBLE
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                txtHome.visibility = View.INVISIBLE
                recyclerView.visibility = View.VISIBLE
                    this@MainActivity.adapter.filter.filter(newText)
                return true
            }
        })

        return true
    }

}
