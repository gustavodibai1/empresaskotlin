package com.example.empresasandroidkotlin.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.empresasandroidkotlin.dagger.ApplicationComponent
import com.example.empresasandroidkotlin.dagger.ViewModelFactory
import javax.inject.Inject

open class BaseActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    protected val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        (application as App).appComponent
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)
    }
}