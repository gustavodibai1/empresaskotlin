package com.example.empresasandroidkotlin.view.listeners

import android.view.View
import com.example.empresasandroidkotlin.data.entity.Enterprise

interface EnterpriseClickListener {
    fun clickItem(view : View?, enterprise : Enterprise)
}