package com.example.empresasandroidkotlin.view.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.empresasandroidkotlin.data.Result
import com.example.empresasandroidkotlin.data.entity.ConfigurationModel
import com.example.empresasandroidkotlin.data.entity.Enterprise
import com.example.empresasandroidkotlin.data.repository.EnterpriseRepository
import com.example.empresasandroidkotlin.view.listeners.GetEmpresasCallback
import com.example.empresasandroidkotlin.util.SharedPreferencesHelper
import com.example.empresasandroidkotlin.view.ViewState
import com.example.empresasandroidkotlin.view.ui.login.LoginViewState
import kotlinx.coroutines.*
import javax.inject.Inject

class EnterpriseViewModel @Inject constructor
    (val repo : EnterpriseRepository)
    : ViewModel() {

    val enterpriseState: MutableLiveData<ViewState<MutableList<Enterprise>, EnterpriseViewState>> = MutableLiveData()

    fun observeEnterpriseState(): MutableLiveData<ViewState<MutableList<Enterprise>, EnterpriseViewState>> = enterpriseState

    fun mostrarEmpresas(
        helper : SharedPreferencesHelper
    ) {
        GlobalScope.launch (Dispatchers.IO) {
            val result = repo.mostrarEmpresas(helper)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success -> enterpriseState.postValue(
                        ViewState(
                            EnterpriseViewState.SUCCESS,
                            result.data
                        )
                    )
                    is Result.Error -> enterpriseState.postValue(
                        ViewState(
                            EnterpriseViewState.ERROR,
                            error = result.exception
                        )
                    )
                    is Result.NetworkError -> enterpriseState.postValue(ViewState(EnterpriseViewState.NETWORK_ERROR))
                }
            }

        }
    }
}