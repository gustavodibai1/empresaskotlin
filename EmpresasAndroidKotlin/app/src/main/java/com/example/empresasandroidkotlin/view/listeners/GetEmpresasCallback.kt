package com.example.empresasandroidkotlin.view.listeners

import com.example.empresasandroidkotlin.data.entity.Enterprise
import java.lang.Exception

interface GetEmpresasCallback {
    fun onError(exception: Exception)
    fun onSuccess(list: MutableList<Enterprise>)
}