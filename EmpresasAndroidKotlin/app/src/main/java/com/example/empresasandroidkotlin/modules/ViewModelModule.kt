package com.example.empresasandroidkotlin.modules

import com.example.empresasandroidkotlin.view.ui.login.LoginViewModel
import com.example.empresasandroidkotlin.view.ui.main.EnterpriseViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val viewModelModule = module {

    viewModel {
        LoginViewModel(get())
    }

    viewModel {
        EnterpriseViewModel(get())
    }

}