package com.example.empresasandroidkotlin.view.ui.main

enum class EnterpriseViewState {
    LOADING, SUCCESS, ERROR, NETWORK_ERROR
}